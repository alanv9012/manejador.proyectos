const express = require('express');
const Users = require('../models/users');
const bcrypt = require('bcrypt');
const async = require('async');


// RESTFULL => GET, POST, PUT, PATCH, DELETE
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    let page = req.params.page ? req.params.page : 1;
    Users.paginate({},{page:page, limit:3}).then(objs => res.status(200).json({
        message: res.__('ok.usersList'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersList'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Users.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.usersIndex'),
        oj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersIndex'),
        obj: ex
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    sync.parallel({
        salt:(callback)=>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result) => {
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new User({
                _name:name,
                _lastName:lastName,
                _email:email,
                _password:hash,
                _salt:result.salt
            });

            user.save().then(obj => res.status(200).json({
                message: res.__('ok.usersCreate'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.usersCreate'),
                obj:ex
            }));
        })
    });
}



function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let email = req.body.email ? req.body.email: "";
    let password = req.body.password ? req.body.password: "";

    let users = new Object({
        _name:name,
        _lastName:lastName,
        _email:email,
        _password:password
    });

    Users.findOneAndUpdate({"_id":id}, users).then(obj => res.status(200).json({
        message: res.__('ok.usersReplace'),
        oj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersReplace'),
        obj: ex
    }));
}

function edit(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    let users = new Object();

    if(name){
        users._name = name;
    }
    if(lastName){
        users._lastName = lastName;
    }
    if(email){
        users._email = email;
    }
    if(password){
        users._password = password;
    }

    Users.findOneAndUpdate({"_id":id}, users).then(obj => res.status(200).json({
        message: res.__('ok.usersEdit'),
        oj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersEdit'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Users.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.usersDestroy'),
        oj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.usersDestroy'),
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}
