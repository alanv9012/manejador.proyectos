const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const mongoose = require('mongoose');
const expressjwt = require('express-jwt');
const config = require('config');
const i18n = require('i18n');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const storiesRouter = require('./routes/userStories');
const membersRouter = require('./routes/teamMembers');
const skillsRouter = require('./routes/skills');
const recordsRouter = require('./routes/projectRecords');
const controlPanelRouter = require('./routes/controlPanels');
const columnsRouter = require('./routes/columns');

const uri = config.get("dbchain");
mongoose.connect(uri);

const db = mongoose.connection;

db.on('error', ()=>{
  console.log("No se a podido conectar a la db")
});

db.on('open', ()=>{
  console.log('Connected to MongoDB');
});

i18n.configure({
  locales:['es','en'],
  cookie: 'language',
  directory: `${__dirname}/locales`
});

const app = express();

app.use(i18n.init);
const jwtKey = config.get("secret.key");
app.use(expressjwt({secret:jwtKey, algorithms:['HS256']})
.unless({path:["/index"]}))
console.log()

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/userStories', storiesRouter);
app.use('/teamMembers', membersRouter);
app.use('/skills', skillsRouter);
app.use('/projectRecords', recordsRouter);
app.use('/controlPanels', controlPanelRouter);
app.use('/columns', columnsRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
