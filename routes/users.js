const express = require('express');
const router = express.Router();

const controller = require('../controllers/users');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('List of users: ');
});

router.get('/:id', controller.getAll);

router.post('/', controller.create);

router.put('/:id', controller.replace);

router.patch('/:id', controller.edit);

router.delete('/:id', controller.destroy);

module.exports = router;
